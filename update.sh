#!/bin/bash

SECONDS=0
INSTANCE=mstdn.tsukiyono.blue
REPOSITORY=https://gitlab.com/tsukiyono-blue/mastodon
DOCKERREPO=registry.gitlab.com/tsukiyono-blue/mastodon

cd ../mastodon

while getopts :mh argument; do
case $argument in
        m) major=true ;;
        h) hub=true ;;
        d) dev=true ;;
        r) rm=true ;;
        *) echo "正しくない引数が指定されました。" 1>&2
           exit 1 ;;
esac
done

if [ "$dev" = "true" ]; then
        GITBRANCH=develop
        DOCKERTAG=develop
else
        GITBRANCH=master
        DOCKERTAG=latest
fi

COMMITHASH=$(git ls-remote ${REPOSITORY}.git ${GITBRANCH} | head -c 7)

echo "[${COMMITHASH}] UPDATE: ${REPOSITORY}/tree/${COMMITHASH}" | toot --visibility unlisted
git fetch
git reset --hard origin/${GITBRANCH}


if [ "$hub" = "true" ]; then
        echo "[${COMMITHASH}] CONTAINER PULL START." | toot --visibility unlisted
        docker pull ${DOCKERREPO}:${DOCKERTAG}
        imageid=`docker images ${DOCKERREPO}:${DOCKERTAG} --format "{{.ID}}" | awk 'END{print}'`
        sed -ie s/'build: .'/'#build: .'/ docker-compose.yml
        echo "[${COMMITHASH}] CONTAINER PULL END. DOCKER IMAGE ID: ${imageid}" | toot --visibility unlisted
else
        echo "[${COMMITHASH}] BUILD START." | toot --visibility unlisted
        docker-compose build
        echo "[${COMMITHASH}] BUILD END." | toot --visibility unlisted
fi

if [ "$major" = "true" ]; then
        echo "[${COMMITHASH}] PREPROCESSING." | toot --visibility unlisted
        docker-compose run --rm -e SKIP_POST_DEPLOYMENT_MIGRATIONS=true web rails db:migrate

        if [ "$rm" = "true" ]; then
                docker-compose stop
                docker-compose rm nginx -f
                docker-compose up -d
        else
                docker-compose up -d
        fi

        while true; do
                sleep 5s
                DonAlive=$(curl -s -o /dev/null -I -w "%{http_code}\n" https://${INSTANCE}/)
                if [ $DonAlive -eq 302 ]; then
                        break
                fi
                        echo "CHECK FAILD: Retry after 5 sec."
        done
fi

docker-compose run --rm web rails db:migrate
docker-compose run --rm web bin/tootctl cache clear

echo "[${COMMITHASH}] DEPLOY." | toot --visibility unlisted
if [ "$rm" = "true" ]; then
        docker-compose stop
        docker-compose rm nginx -f
        docker-compose up -d
else
        docker-compose up -d
fi

while true; do
        sleep 5s
        DonAlive=$(curl -s -o /dev/null -I -w "%{http_code}\n" https://${INSTANCE}/)
        if [ $DonAlive -eq 302 ]; then
                break
        fi
        echo "CHECK FAILD: Retry after 5 sec."
done

echo "[${COMMITHASH}] CLEANING." | toot --visibility unlisted
docker container prune -f
docker image prune -f

VERSION=$(curl -s https://${INSTANCE}/api/v1/instance | jq -r '.version')
TIME=$(date -u -d @${SECONDS} +"%T")
echo "[${COMMITHASH}] ${VERSION} ✅ UPDATE TIME: $TIME" | toot --visibility unlisted
